# CMS Data Analysis School (DAS) - Statistics Short Exercise

## Introduction

This is a set of tutorials for the [CMS Data Analysis School (DAS) at CERN 2024](https://indico.cern.ch/event/1388937) intended to cover the [Statistics Short Exercise](https://indico.cern.ch/event/1388937/timetable/). In these exercises, you will learn how to use the statistical inference tools employed in CMS. This software is, to a large extent, a derivation of the statistical analysis toolkits [`RooFit`](https://root.cern/manual/roofit/) and [`RooStats`](https://twiki.cern.ch/twiki/bin/view/RooStats/WebHome). With this knowledge, you will be able to perform some standard, but very widely used, statistical procedures (e.g. parameter estimation, interval confidence level calculation, testing of statistical hypotheses, etc). You will learn how to use the CMS tool [`Combine`](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/latest/), which, as its name suggests, can be used to perform a variety of these tasks when conducting a combination of multiple results. 

## Facilitators

 1. Aliya Nigamova (HH/DESY)
 2. Fabio Monti (CERN)
 3. Kyle Cormier (UZH)
 4. Danyer Perez Adan (RWTH)


## Main notebooks in this tutorial

 0. [`setup-libraries.ipynb`](setup-libraries.ipynb): setting up libraries using `CMSSW`
 1. [`0/exercise_0.ipynb`](0/exercise_0.ipynb): a very short `PyROOT`/`RooFit` tutorial
 2. [`1/exercise_1.ipynb`](1/exercise_1.ipynb): analyzing a single count using `RooFit`/`RooStats` 
 3. [`2/exercise_2.ipynb`](2/exercise_2.ipynb): analyzing three counts using `RooFit`/`RooStats`
 4. [`3/exercise_3a.ipynb`](3/exercise_3a.ipynb): analyzing three counts using `combine`
 5. [`3/exercise_3b.ipynb`](3/exercise_3b.ipynb): estimating limits in a low count experiment  using `combine`
 6. [`3/exercise_3c.ipynb`](3/exercise_3c.ipynb): systematic uncertainties for different background estimates using `combine`
 7. [`4/exercise_4.ipynb`](4/exercise_4.ipynb): binned fit to Run I H->gg data
 8. [`5/exercise_5.ipynb`](5/exercise_5.ipynb): unbinned fit to Type Ia supernovae distance modulus/red shift data (optional)
 9. [`6/exercise_6.ipynb`](6/exercise_6.ipynb): histogram template analysis using `combine` (optional)
 
## Setup

We will be using the [SWAN](https://swan.web.cern.ch/swan/) (Service for Web based ANalysis), in particular, its Jupyter notebook interface. 

The procedure to set up SWAN is the similar the one that you followed for the pre-exercises:
  - Go to [https://swan.cern.ch/](https://swan.cern.ch/) and select the default configuration to start your session
  - Click on the cloud icon to download/clone 
  ![Preview](https://gitlab.cern.ch/cmsdas-cern-2024/pyROOTforCMSDAS/-/raw/master/pictures/cloud.png) 
  - Copy the Git `HTTPS` link (i.e. `https://gitlab.cern.ch/cmsdas-cern-2024/short-ex-stat.git`) and paste it to download/clone the project
  - **Click on [`setup-libraries.ipynb`](setup-libraries.ipynb) to execute the required `statistics-das-2024` kernel.**

## Links

The indico page is: [https://indico.cern.ch/event/1388937/timetable/](https://indico.cern.ch/event/1388937/timetable/)

The twiki is: <https://twiki.cern.ch/twiki/bin/view/CMS/CMSDASCERN2024StatisticsExercise>
